#------------------------------------------------------------------------------
# General

# Personal
set my_name = "Timothée Floure"

# Cache
set header_cache = ~/.local/var/cache/mutt/
set message_cachedir = ~/.local/var/cache/mutt/messages/
set tmpdir = "$XDG_CACHE_HOME"/neomutt/tmp

# Mails
set mbox_type=Maildir
set folder="~/.local/var/mail/"

# Misc
set alias_file = "$HOME"/.local/var/mail/aliases
set mailcap_path = "$XDG_CONFIG_HOME"/mailcap

# Sending
set realname = 'Timothée Floure'

#------------------------------------------------------------------------------
# Accounts

set mbox_type=Maildir
set mask="*"

mailboxes "=Fnux ========================"
source $XDG_CONFIG_HOME/neomutt/accounts/fnux.muttrc
mailboxes "=EPFL ========================"
source $XDG_CONFIG_HOME/neomutt/accounts/epfl.muttrc
mailboxes "=Inilab ======================"
source $XDG_CONFIG_HOME/neomutt/accounts/inilab.muttrc

# Default folder
source $XDG_CONFIG_HOME/neomutt/accounts/fnux.muttrc

# Account hooks (triggered when changing into a directory of that account):
folder-hook fnux/* "source $XDG_CONFIG_HOME/neomutt/accounts/fnux.muttrc"
folder-hook epfl/* "source $XDG_CONFIG_HOME/neomutt/accounts/epfl.muttrc"
folder-hook inilab/* "source $XDG_CONFIG_HOME/neomutt/accounts/inilab.muttrc"

#------------------------------------------------------------------------------
#View

# Show text-version of mail first
alternative_order text/plain text/enriched text/html

#------------------------------------------------------------------------------
# UI/colors

# Default sorting options
set sort = reverse-threads

# Sidebar
set sidebar_visible
set sidebar_short_path
set sidebar_folder_indent
set sidebar_indent_string = "  "
set sidebar_delim_chars = "/"
set sidebar_format = "%B%?F? [%F]?%* %?N?%N?"
set mail_check_stats

# General
color indicator         yellow default

# Pager
color header            brightgreen default ^From:
color header            brightblue default ^To:
color header            cyan default ^Reply-To:
color header            cyan default ^Cc:
color header            brightwhite default ^Subject:

color body              red default [\-\.+_a-zA-Z0-9]+@[\-\.a-zA-Z0-9]+
color body              cyan default (https?|ftp)://[\-\.,/%~_:?&=\#a-zA-Z0-9]+

# Index
color index_flags       yellow default '.*'
color index_author      white default '.*'
color index_subject     white default '.*'

#------------------------------------------------------------------------------
# Keybindings

# Moving around (vim-like)
bind attach,browser,index       g   noop
bind attach,browser,index       gg  first-entry
bind attach,browser,index       G   last-entry
bind pager                      g   noop
bind pager                      gg  top
bind pager                      G   bottom
bind pager                      k   previous-line
bind pager                      j   next-line
bind index                      l   display-message
macro browser                   l   '<enter>' 'Change to selected folder'
bind index                      h   noop
bind pager                      h   exit
bind pager                      t   display-toggle-weed

# Sidebar
bind index,pager                B   sidebar-toggle-visible
bind index,pager                K   sidebar-prev
bind index,pager                J   sidebar-next
bind index,pager                L   sidebar-open
bind index,pager                <up>    sidebar-prev
bind index,pager                <down>  sidebar-next
bind index,pager                <right> sidebar-open

# Misc
bind index                      ?   search-reverse
bind index                      z   collapse-thread
bind index                      Z   collapse-all
bind pager,index                d   noop
bind pager,index                dd  delete-message
macro index,pager c '<change-folder>?<toggle-mailboxes>' 'open a different folder'
macro index A \
    "<tag-pattern>~O<enter><tag-prefix><clear-flag>O<untag-pattern>.<enter>" \
    "mark all old as read" 
bind index,pager                r   group-reply
#------------------------------------------------------------------------------
# Misc

# Notification(s)
# set new_mail_command="notify-send 'New Email' '%n new messages, %u unread.' &"

# Use GPG2 instead of gpg + PGP configuration
source $XDG_CONFIG_HOME/neomutt/pgp.muttrc
set pgp_use_gpg_agent = yes
set pgp_sign_as = 331ED8E6
set pgp_timeout = 3600
set crypt_autosign = yes
set crypt_replyencrypt = yes
